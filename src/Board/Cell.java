import java.util.EnumMap;
import java.util.Map;
import java.util.Objects;

// Cell on Field
public class Cell {
    // Field for hitted cell check
    private boolean isHitted = false;

    // Unit that's inside the cell
    private Unit unit;
    private UnitType unitType = UnitType.NONE;

    // Neighbour cells
    private final Map<Direction, Cell> neighborCells = new EnumMap<>(Direction.class);

    // Constructor
    public Cell() {}

    // Get neighbor cells map
    public final Map<Direction, Cell> getNeighborCells() {
        return neighborCells;
    }

    public Cell getNeighborCell(Direction direction) { return neighborCells.get(direction); }

    // Set neighbor by direction
    void setNeighbor(Cell neighborCell, Direction direction) {
        if (neighborCell == this || neighborCells.containsKey(direction) || neighborCells.containsValue(neighborCell)) {
            System.out.println("Argument cell can't be set as neighbour!");
            throw new IllegalArgumentException("Argument cell can't be set as neighbour!");
        }

        neighborCells.put(direction, neighborCell);
        if (neighborCell.getNeighborCell(direction.getOppositeDirection()) == null) {
            neighborCell.setNeighbor(this, direction.getOppositeDirection());
        }
    }

    public void hit() {
        isHitted = true;
    }

    public boolean isHitted() {
        return isHitted;
    }

    public UnitType getUnitType() {
        return unitType;
    }

    public Unit getUnit() {
        if(Objects.isNull(unit)) {
            System.out.println("Cell's unit is null!");
            throw new RuntimeException("Cell's unit is null!");
        }

        return unit;
    }

    public void setUnit(UnitType unit) {
        switch (unit){
            case SHIP_PART:
                this.unit = new ShipPart();
                unitType = UnitType.SHIP_PART;
                break;
            case BROKEN_SHIP_PART:
                this.unit = new BrokenShipPart();
                unitType = UnitType.BROKEN_SHIP_PART;
                break;
            case NAVAL_MINE:
                this.unit = new NavalMine();
                unitType = UnitType.NAVAL_MINE;
                break;
            case ISLAND:
                this.unit = new Island();
                unitType = UnitType.ISLAND;
                break;
            case LIGHTHOUSE:
                this.unit = new Lighthouse();
                unitType = UnitType.LIGHTHOUSE;
                break;
            case NONE:
                this.unit = null;
                unitType = UnitType.NONE;
                break;
        }
    }
}
