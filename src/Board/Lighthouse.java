import java.util.ArrayList;
import java.util.Map;

public class Lighthouse extends NavalMine {
    private int power = 3;

    public static ArrayList<Cell> getAllLightedCells(Cell cell, int power) {
        System.out.println("Called getAllLightedCells");
        System.out.println("Called again LR3");
        if(power < 1)
            throw new GameCreationException("Lighthouse Power can't be less than 1!");

        ArrayList<Cell> cellsToLightUp = new ArrayList<>();

        cellsToLightUp.addAll(getNumberOfCellsInDir(cell, Direction.NORTH, power));
        cellsToLightUp.addAll(getNumberOfCellsInDir(cell, Direction.EAST, power));
        cellsToLightUp.addAll(getNumberOfCellsInDir(cell, Direction.WEST, power));
        cellsToLightUp.addAll(getNumberOfCellsInDir(cell, Direction.SOUTH, power));

        ArrayList<Cell> iterable = new ArrayList<>();
        for (Cell item : cellsToLightUp) iterable.add(item);


        iterable.forEach(c -> {
            for (Map.Entry<Direction, Cell> entry : c.getNeighborCells().entrySet()) {
                if(!cellsToLightUp.contains(entry.getValue())) {
                    cellsToLightUp.add(entry.getValue());
                }
            }
        });

        //
        cellsToLightUp.forEach(c -> {
            checkLighted(c);
        });

        return cellsToLightUp;
    }

    private static ArrayList<Cell> getNumberOfCellsInDir(Cell cell, Direction dir, int length) {
        System.out.println("Called getNumberOfCellsInDir");
        System.out.println("Called again LR3");
        ArrayList<Cell> returnStatement = new ArrayList<>();

        while(cell.getNeighborCell(dir) != null && length > 1) {
            returnStatement.add(cell.getNeighborCell(dir));
            cell = cell.getNeighborCell(dir);

            length--;
        }

        return returnStatement;
    }

    private static void checkLighted(Cell c) {
        System.out.println("Called checkLighted");
        System.out.println("Called again LR3");
        if(c.getUnitType() != UnitType.SHIP_PART && c.getUnitType() != UnitType.BROKEN_SHIP_PART && c.getUnitType() != UnitType.NAVAL_MINE && c.getUnitType() != UnitType.ISLAND && c.getUnitType() != UnitType.LIGHTHOUSE) {
            c.hit();
        }
    }

    public void setPower(int power) {
        System.out.println("Called setPower");
        System.out.println("Called again LR3");
        this.power = power;
    }

    public int getPower() {
        return power;
    }
}
