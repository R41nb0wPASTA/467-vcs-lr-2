public enum Direction {
    NORTH,
    EAST,
    WEST,
    SOUTH;

    /**
     * Opposite direction
     */
    private Direction opposite;

    static  {
        NORTH.opposite = SOUTH;
        EAST.opposite = WEST;
        WEST.opposite = EAST;
        SOUTH.opposite = NORTH;
    }

    public Direction getOppositeDirection() {
        return opposite;
    }
}
