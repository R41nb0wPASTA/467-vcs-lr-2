public class GameCreationException extends RuntimeException {
    public GameCreationException (String message) { super(message); }
}
