import javax.swing.*;
import java.awt.*;

public class    CellWidget extends CustomJButton {
    private final Cell cell;
    private PlayerActionListener playerActionListener;

    // for AI player's field
    public CellWidget(Cell cell, Color mainColor, Color hoveredColor){
        super("", mainColor, hoveredColor);

        this.cell = cell;

        setPreferredSize(new Dimension(50, 50));

        addActionListener(e -> this.fireCellClicked());
        //Step 1
    }

    // for Alive player's field
    public CellWidget(Cell cell, Color mainColor){
        super("", mainColor, mainColor);
        UIManager.put( "Button.select", mainColor );
        this.cell = cell;

        setPreferredSize(new Dimension(50, 50));

        system.out.println();

        // Step 2
    }

    public Cell getCell() {
        return cell;
        //Step 3
    }

    //Check Comment Step 3
    public void setListener(PlayerActionListener listener){
        playerActionListener = listener;
    }

    private void fireCellClicked(){
        PlayerActionEvent event = new PlayerActionEvent(this);
        event.setCell(cell);

        playerActionListener.playerFieldClick(event);
    }

    // Empty Method for <my branch>
    private void patheticMethod(){}

    //Dont push this shit
    private void realShit(){
        //Step 5
    }
}
