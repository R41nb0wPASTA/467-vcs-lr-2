import javax.swing.*;
import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class GameMenuWidgetModal extends JDialog {
    private final WindowHandler owner;
    private final JTextField aliveName = new JTextField("PASTA");
    private final JSpinner fieldSize = new JSpinner(new SpinnerNumberModel(10, 10, 13/*15*/, 1));

    private final CustomModal incorrectInputModal;

    public GameMenuWidgetModal(WindowHandler owner){
        System.out.println("new");
        super(owner, "SekaShips", true);

        if(owner == null)
            throw new IllegalArgumentException("WindowHandler can't be null!");
        this.owner = owner;

        //Turn off program by X from this modal window
        this.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                System.exit(0);
            }
        });

        setLocation(center(350, 245));
        setResizable(false);
        setSize(new Dimension(350, 245));
        getContentPane().setBackground(Styles.PRIMARY_BACKGROUND_COLOR);

        this.aliveName.setFont(Styles.PRIMARY_FONT);
        this.fieldSize.setFont(Styles.PRIMARY_FONT);

        int gridCounter = 0;
        setLayout(new GridBagLayout());
        GridBagConstraints constraints = new GridBagConstraints();
        constraints.insets = new Insets(3,30,5,30);
        constraints.fill = GridBagConstraints.BOTH;
        constraints.weightx = 1.0;
        constraints.gridy = gridCounter++;
        constraints.gridx = 0;
        constraints.anchor = GridBagConstraints.CENTER;

        JSeparator divider = new JSeparator(SwingConstants.HORIZONTAL);
        divider.setBackground(Styles.SEA_COLOR);

        //Header for name settings
        constraints.gridwidth = 2;
        constraints.gridx = 0;
        add(divider, constraints);
        constraints.gridy = gridCounter++;
        JLabel namesHeader = CustomJLabel.createCustomLabel("Names", Styles.MENU_HEADER_FONT);
        add(namesHeader, constraints);
        constraints.gridy = gridCounter++;

        //Alive player name input
        constraints.gridwidth = GridBagConstraints.REMAINDER;
        constraints.gridx = 0;
        JLabel alivePlayerLabel = CustomJLabel.createCustomLabel("Your name (1-5 chars): ", Styles.PRIMARY_FONT);
        add(alivePlayerLabel, constraints);
        constraints.gridx = 1;
        add(this.aliveName, constraints);
        constraints.gridy = gridCounter++;

        //Header for game settings
        constraints.gridwidth = 2;
        constraints.gridx = 0;
        add(divider, constraints);
        constraints.gridy = gridCounter++;
        JLabel sizeHeader = CustomJLabel.createCustomLabel("Settings", Styles.MENU_HEADER_FONT);
        add(sizeHeader, constraints);
        constraints.gridy = gridCounter++;

        //Field size input form
        constraints.gridwidth = 1;
        constraints.gridx = 0;
        JLabel fieldSizeLabel = CustomJLabel.createCustomLabel("Field size (10 - 13): ", Styles.PRIMARY_FONT);
        add(fieldSizeLabel, constraints);
        constraints.gridx = 1;
        add(this.fieldSize, constraints);
        constraints.gridy = gridCounter++;

        //Start button
        constraints.gridwidth = 2;
        constraints.anchor = GridBagConstraints.CENTER;
        constraints.gridx = 0;
        constraints.gridy = gridCounter;

        CustomJButton startBtn = new CustomJButton("START", Styles.REGULAR_BUTTON, Styles.REGULAR_HOVERED_BUTTON);
        startBtn.addActionListener(e -> handleStart());
        add(startBtn, constraints);

        //Modal window for invalid input
        JLabel incorrectInputText = CustomJLabel.createCustomLabel("Incorrect input!", Styles.MENU_HEADER_FONT);
        incorrectInputText.setFont(Styles.PRIMARY_FONT);
        this.incorrectInputModal = new CustomModal(this.owner, "Error!", incorrectInputText, 200,110);
        CustomJButton incorrectButtOk = new CustomJButton("OK", Styles.REGULAR_BUTTON, Styles.REGULAR_HOVERED_BUTTON);
        incorrectButtOk.addActionListener(e -> incorrectInputModal.setVisible(false));
        this.incorrectInputModal.addButton(incorrectButtOk);
    }

    private void handleStart(){
        //new comment
        if(validateInput()) {
            this.owner.runGame((int)fieldSize.getValue(), aliveName.getText());
            this.setVisible(false);
        }
        else
            this.incorrectInputModal.setVisible(true);
        //коментарий 2
    }

    private boolean validateInput() {
        //another comment
        Pattern pattern = Pattern.compile("^.{1,5}$");
        Matcher matcher = pattern.matcher(aliveName.getText());

        return matcher.find();
    }

    public Point center(int width, int height) {
        //another another comment
        Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();

        int x = (dim.width - width) / 2;
        int y = (dim.height - height) / 2;

        return new Point(x, y);
    }
}